Categories:System
License:GPL-3.0+
Web Site:http://quaap.com/D/LaunchTime
Source Code:https://github.com/quaap/LaunchTime
Issue Tracker:https://github.com/quaap/LaunchTime/issues
Donate:https://paypal.me/quaap

Auto Name:LaunchTime
Summary:Organize your homescreen and apps
Description:
Alternative homescreen which features a side menu used to organize your apps
into common-sense and configurable categories. It also includes widgets, text
search for apps, a QuickBar, links/shortcuts, recent apps list, and portrait and
landscape support.

This is the initial alpha release.
.

Repo Type:git
Repo:https://github.com/quaap/LaunchTime

Build:0.1,1
    commit=v0.1
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.1
Current Version Code:1
